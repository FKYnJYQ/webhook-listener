#[macro_use]
extern crate log;
use std::process::Command;
use std::path::Path;
extern crate env_logger;
//extern crate dotenv;

//use dotenv::dotenv;
//use std::env;

mod clap_config;

use actix_web::{web, App, HttpServer, HttpRequest, HttpResponse, http::header::HeaderMap};


struct Config {
    secret: String,
    bash_file_path: String,
}

impl Config {
    fn execute_file(&self) {
        Command::new("bash")
            .arg(self.bash_file_path.clone())
            .spawn()
            .expect("failed to execute process");
    }
}

fn verify(headers: &HeaderMap, state: &String) -> bool {
    match headers.get("X-Gitlab-Token") {
        Some(value) => { value.to_str().unwrap() == state }
        None => false
    }
}

fn resp(data: web::Data<Config>, req: HttpRequest) -> HttpResponse {
    if verify(req.headers(), &data.secret) {
        info!("pass verification, execute bash script.");
        data.execute_file();
        HttpResponse::Ok().into()
    } else {
        info!("verification error, return.");
        HttpResponse::Unauthorized().into()
    }
}

fn main() -> std::io::Result<()> {
//    dotenv().ok();
    env_logger::init();

    let matches = clap_config::new_matches();
    let port = matches.value_of("port").unwrap_or("8000");
    let secret = matches.value_of("token").unwrap();
    let bash_file_path = matches.value_of("file").unwrap();

    let config = Config{
        secret:String::from(secret),
        bash_file_path: String::from(bash_file_path)
    };

    // panic if file not exist
    if let false = Path::new(bash_file_path).exists() {
        panic!("file {} does not exists!", bash_file_path);
    }

    let secret = web::Data::new(config);

    HttpServer::new(
        move || App::new()
            .register_data(secret.clone())
            .service(web::resource("/").to(resp))
//            .service(web::resource("/test/").to(index))
    )
        .bind(format!("0.0.0.0:{}", port))?
        .run()
}
