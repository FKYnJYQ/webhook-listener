use clap::ArgMatches;
use clap::clap_app;


/// clap CLI config, using macro
pub fn new_matches() -> ArgMatches<'static>{
    clap_app!(myapp =>
    (version: "0.2")
    (author: "FKYnJYQ <loveress01@gmail.com>")
    (about: "Simple webhook trigger server")
    (@arg token: -t --token +takes_value "provide a secret phrase as X-Gitlab-Token")
    (@arg port: -p --port +takes_value "port to listien")
    (@arg file: -f --file +takes_value "bash file to execute")
).get_matches()
}